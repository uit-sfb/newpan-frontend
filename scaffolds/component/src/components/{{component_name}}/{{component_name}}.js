// React
import React, { PropTypes } from 'react';
import {Panel, Button, Collapse} from 'react-bootstrap';

// Actions
import AppActions from '../../actions/AppActions.js';

// Components

// API
import ApiUtils from '../../utils/WebApiUtils.js';

// Stores



let {{component_name}} = React.createClass({

  render() {
    return(
      <div>
      Hello from {{component_name}}
      </div>
    );
  }
});

export default {{component_name}};
