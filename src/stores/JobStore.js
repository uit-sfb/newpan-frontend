import EventEmitter from 'eventemitter3';
import Dispatcher from '../core/Dispatcher';
import ActionTypes from '../constants/ActionTypes';

const CHANGE_EVENT = 'change';

var state = {
  jobs: {
    /*'a' : {name: 'Meta-pipe 1', created: 1, state: 'submitted'},
    'b' : {name: 'Meta-pipe 2', created: 2, state: 'delayed'},
    'c' : {name: 'Meta-pipe 3', created: 3, state: 'finished'},
    'd' : {name: 'Meta-pipe 4', created: 4, state: 'finished'},
    'e' : {name: 'Meta-pipe 5', created: 5, state: 'finished'},
    'f' : {name: 'Meta-pipe 6', created: 6, state: 'finished'}*/
  },
  submittingJobs: {
    x: {name: 'Submitting 1', state: 'submitting'}
  }
};

let stateDescriptions = {
  'submitted': 'Job submitted. You will be notified when the result ready.',
  'finished': 'The analysis is complete.', //todo: more info
  'delayed': 'The analysis has been delayed due to an unexpected event. You will be notified when the result is ready.',
  'submitting': 'Submitting...'
};

let exportJob = (id, job) => {
  let exp = {
    name: job.name,
    created: job.timeSubmitted,
    state: job.state,
    id: id,
    inputUrls: job.inputs,
    outputUrls: job.outputs
  };

  exp.description = stateDescriptions[job.state.toLowerCase()];

  return exp;
};

var JobStore = Object.assign({}, EventEmitter.prototype, {
  emitChange() {
    return this.emit(CHANGE_EVENT);
  },

  onChange(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  off(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },

  getJobs() {
    let res = JSON.parse(JSON.stringify(state.jobs));
    return res;
  }
});

JobStore.dispatchToken = Dispatcher.register((action) => {
  var emitChange = true;

  switch (action.type) {
    case ActionTypes.JOBS_REFRESH:
      for(var i = 0; i < action.jobs.length; i++) {
        try {
          var job = action.jobs[i];
          job.name = job.properties.name;

          if(typeof job.name === 'undefined') {
            job.name = "Annotations";
          }

          state.jobs[job.jobId] = exportJob(job.jobId, job);
        } catch (e) {
          console.log(e)
        }
      }
      break;
    default:
      emitChange = false;
  }

  if (emitChange === true) {
    JobStore.emitChange();
  }

});

export default JobStore;
