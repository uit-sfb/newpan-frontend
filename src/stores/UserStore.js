import EventEmitter from 'eventemitter3';
import Dispatcher from '../core/Dispatcher';
import ActionTypes from '../constants/ActionTypes';

const CHANGE_EVENT = 'change';

var state = {
  signingIn: false,
  user: null
};

var UserStore = Object.assign({}, EventEmitter.prototype, {
  emitChange() {
    return this.emit(CHANGE_EVENT);
  },

  onChange(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  off(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },

  getState() {
    return state;
  }
});

UserStore.dispatchToken = Dispatcher.register((action) => {
  var emitChange = true;

  switch (action.type) {
    case ActionTypes.SIGNING_IN:
          state.signingIn = true;
          break;
    case ActionTypes.SIGNED_OUT:
          state.user = null;
          break;
    case ActionTypes.SIGNED_IN:
          state.user = action.user;
          state.signingIn = false;
          console.log(state);
          break;
    case ActionTypes.SIGNED_IN_FAILED:
          state.user = null;
          state.signingIn = false;
          break;
    default:
          emitChange = false;
  }

  if (emitChange === true) {
    UserStore.emitChange();
  }

});

export default UserStore;
