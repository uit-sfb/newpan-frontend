import EventEmitter from 'eventemitter3';
import Dispatcher from '../core/Dispatcher';
import ActionTypes from '../constants/ActionTypes';

const CHANGE_EVENT = 'change';

var state = {
  /*datasets: {
    'a' : {name: 'Dataset 1', description: 'Hello world dataset', created: 1},
    'b' : {name: 'Dataset 2', description: 'some nice ... fasta', created: 2},
    'c' : {name: 'Dataset 1', description: 'Hello world dataset', created: 3},
    'd' : {name: 'Dataset 1', description: 'Hello world dataset', created: 4},
    'e' : {name: 'Dataset 1', description: 'Hello world dataset', created: 5},
    'f' : {name: 'Dataset 1', description: 'Hello world dataset', created: 6}
  },*/
  datasets: {

  },
  uploadProgress: {

  }
};

var DatasetStore = Object.assign({}, EventEmitter.prototype, {
  emitChange() {
    return this.emit(CHANGE_EVENT);
  },

  onChange(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  off(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },

  getDatasets() {
    let res = {};

    for (var uploadKey in state.uploadProgress) {
      if(state.uploadProgress.hasOwnProperty(uploadKey)) {
        res[uploadKey] = state.uploadProgress[uploadKey];
      }
    }

    for (var key in state.datasets) {
      if(state.datasets.hasOwnProperty(key)) {
        let dataset = state.datasets[key];
        res[key] = {
          name: dataset.name,
          description: dataset.description,
          created: dataset.created,
          id: key,
          tags: dataset.tags,
          url: key
        }
      }
    }
    return res;
  }
});

DatasetStore.dispatchToken = Dispatcher.register((action) => {
  var emitChange = true;

  switch (action.type) {
    case ActionTypes.DATASET_REFRESH:
      state.datasets = action.datasets;
      break;
    case ActionTypes.UPLOAD_PROGRESS:
      let dataset = action.dataset;
      state.uploadProgress[dataset.id] = dataset;
      break;
    default:
      emitChange = false;
  }

  if (emitChange === true) {
    DatasetStore.emitChange();
  }

});

export default DatasetStore;
