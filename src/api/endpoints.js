import { Router } from 'express';
import fs from 'fs';
import path from 'path';

const router = new Router();

const CONFIG_DIR = path.join(__dirname, './config');

router.get('/', async (req, res, next) => {
  fs.readFile(path.join(CONFIG_DIR, 'endpoints.json'), {encoding: 'utf8'}, (err, data) => {
    if (err) {
      res.status(500).send(err)
    } else {
      res.status(200).send(JSON.parse(data));
    }
  });
});

export default router;
