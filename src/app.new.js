import 'babel/polyfill';
import React from 'react';
import FastClick from 'fastclick';
import App from './components/App';
import Dispatcher from './core/Dispatcher';
import AppActions from './actions/AppActions';
import ActionTypes from './constants/ActionTypes';

import router from './router.js'

var Router = require('react-router');

var Route = Router.Route;
var RouteHandler = Router.RouteHandler;
/*
var Container = React.createClass({
  render() {
    return (
      <div>
        <h1>hello</h1>
        <RouteHandler/>
      </div>
    );
  }
});

var Inbox = React.createClass({
  render() {
    return (
      <div>
        <h1>Inbox</h1>
        <RouteHandler/>
      </div>
    );
  }
});

var Email = React.createClass({
  render() {
    return (
      <div>
        <h1>Mail: {this.props.params.id}</h1>
      </div>
    );
  }
});*/

//router.run(document.body);
router({hello: 'world'}).run((Root) => {
  React.render(<Root/>, document.body);
});

/*
var routes = (
  <Route handler={App}>
    <Route path="inbox" handler={Inbox}>
      <Route path="messages/:id" handler={Email} />
    </Route>
  </Route>
);

var router = Router.create({
  routes: routes,
  location: Router.HistoryLocation
});

router.run((Root) => {
  React.render(<Root/>, document.body);
});
*/

/*Router.run(routes, Router.HashLocation, (Root) => {
  React.render(<Root/>, document.body);
});*/
