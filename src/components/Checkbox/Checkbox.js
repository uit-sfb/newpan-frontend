/**
 * Created by tteige on 26.05.17.
 */

import React, {Proptypes} from 'react'

class Checkbox extends React.Component {

  constructor(props) {
    super(props);
    this.state = {checked: this.props.defaultChecked};
    if(this.props.onClick) {
      this.onClick = this.props.onClick;
    } else {
      this.onClick = this.onHandleClick.bind(this);
    }

  }

  onHandleClick(event) {
    let newState = this.state;
    newState.checked = !newState.checked;
    this.setState(newState);
    event.target.value = newState.checked;
  }

  render () {
    return (
      <div className='checkbox' style={this.props.style}>
        <label class='sr-only' for={this.props.id}>
          <input type='checkbox' id={this.props.id} onClick={this.onClick} defaultChecked={this.props.defaultChecked}/>
          {this.props.label}
        </label>
      </div>
    );
  }

};

export default Checkbox;
