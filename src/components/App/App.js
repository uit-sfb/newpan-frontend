/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes } from 'react';
import styles from '../variables.less';
import withContext from '../../decorators/withContext';
import withStyles from '../../decorators/withStyles';
import AppActions from '../../actions/AppActions';
import AppStore from '../../stores/AppStore';
import UserStore from '../../stores/UserStore';
import Header from '../Header';
import ContentPage from '../ContentPage';
import ContactPage from '../ContactPage';
import LoginPage from '../LoginPage';
import RegisterPage from '../RegisterPage';
import NotFoundPage from '../NotFoundPage';
import Feedback from '../Feedback';
import Footer from '../Footer';

var Router = require('react-router');
var RouteHandler = Router.RouteHandler;

const pages = { ContentPage, ContactPage, LoginPage, RegisterPage, NotFoundPage };

@withContext
@withStyles(styles)
class App extends React.Component {

  static propTypes = {
    path: PropTypes.string.isRequired
  };

  static childContextTypes = {
    user: React.PropTypes.object
  };


  constructor(props) {
    super(props);
    this.handleUserState = this.handleUserState.bind(this);
    this.state = {user: UserStore.getState()};
  }

  getChildContext() {
    return {
      user: this.state.user
    };
  }

  componentDidMount() {
    window.addEventListener('popstate', this.handlePopState);
    UserStore.onChange(this.handleUserState);

    this.setState({
      userState: UserStore.getState()
    });
  }

  componentWillUnmount() {
    window.removeEventListener('popstate', this.handlePopState);
    UserStore.of(this.handleUserState);
  }

  /*
  shouldComponentUpdate(nextProps) {
    return this.props.path !== nextProps.path;
  }*/

  render() {
    return(
      <div>
        <Header />
        {/*Container to ensure no footer wrapping*/}
        <div className="outerContainer">
          <RouteHandler user={this.state.user}/>
        </div>
        <Footer />
      </div>
    );
  }

  /*render() {
    let component;
    let user = {
      signingIn: false,
      user: null
    };

    switch (this.props.path) {

      case '/':
      case '/about':
      case '/privacy':
        let page = AppStore.getPage(this.props.path);
        component = React.createElement(pages[page.component], page);
        break;

      case '/contact':
        component = <ContactPage />;
        break;

      case '/login':
        component = <LoginPage user={this.state.user} />;
        break;

      case '/register':
        component = <RegisterPage />;
        break;
    }

    return component ? (
      <div>
        <Header />
        {component}
        <Feedback />
        <Footer />
      </div>
    ) : <NotFoundPage />;
  }*/

  handlePopState(event) {
    AppActions.navigateTo(window.location.pathname, {replace: !!event.state});
  }

  handleUserState() {
    this.setState({
      user: UserStore.getState()
    });
    console.log('app: updated state');
    //this.forceUpdate();
  }

}

export default App;
