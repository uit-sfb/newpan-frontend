// React
import React, {PropTypes} from 'react';
import {Panel, Button, Collapse} from 'react-bootstrap';
import {ListGroup, ListGroupItem, Input, ProgressBar, Grid, Col, Row} from 'react-bootstrap'

// Actions
import AppActions from '../../actions/AppActions.js';

// Components
import DownloadButton from '../DownloadButton';

// API
import ApiUtils from '../../utils/WebApiUtils.js';

// Stores

// <DownloadButton dataset={dataset}/>

let DatasetInfo = React.createClass({
  render() {
    let dataset = this.props.dataset;

    if (typeof dataset === 'undefined') {
      dataset = {
        description: '',
        name: ''
      }
    }
    let progressBar;

    if (dataset.description === undefined || dataset.description === '') {
      dataset.description = 'No description for this file';
    }

    let description = (
      <span>
        <h4>{dataset.name}</h4>
        <DownloadButton dataset={dataset}/>
        <small> {dataset.description}</small>
        <span className="pull-right"><a href={dataset.url} onClick={
          function (e) {
            e.preventDefault();
          }
        }><small>dataset url</small></a></span>
      </span>);

    if (dataset.upload) {
      progressBar = <ProgressBar now={Math.floor(dataset.upload.progress * 100)} label='%(percent)s%'/>
    }

    return (
      <span>
        {dataset.upload ? progressBar : description}
      </span>
    );
  }
});
export default DatasetInfo;
