// React
import React, {PropTypes} from 'react';
import {Panel, Button, ListGroup, ListGroupItem, Well, Accordion} from 'react-bootstrap';

// Actions
import AppActions from '../../actions/AppActions.js';
import DatasetFormatUtils from '../../utils/DatasetFormatUtils'

// Components

// API
import ApiUtils from '../../utils/WebApiUtils.js';

// Stores

//Day definitions
let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
//Month definitions
let months = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

let MetapipeJobListItem = React.createClass({

  clickHandler(e) {
    e.preventDefault();
    AppActions.navigateTo(this.href())
  },

  href() {
    return '/metapipe/results/' + this.props.job.id;
  },

  render() {
    let job = this.props.job;
    let style = '';

    switch (job.state) {
      case 'FINISHED':
        style = 'success';
        break;
      case 'DELAYED':
        style = 'warning';
        break;
    }

    var description = job.description;
    var results = <div/>;

    if (job.state === 'FINISHED') {
      let href = this.href();
      results = <a href={href} onClick={this.clickHandler}>Go to results</a>
    }
    let input = [];
    for (let i in job.inputUrls) {
      if (job.inputUrls.hasOwnProperty(i)) {
        let parsed = DatasetFormatUtils.urlToFilename(job.inputUrls[i].url);
        input.push(<div key={parsed}>
          <small>Input: {parsed}</small>
        </div>)
      }
    }

    let date = new Date(Number(job.created));
    return (
      <ListGroupItem header={job.name} bsStyle={style}>
        {description}
        <div>
          <small>Submitted: {date.toISOString()}</small>
        </div>
        {input}
        {results}
      </ListGroupItem>
    );
  }
});

let JobList = React.createClass({
  render() {
    let jobs = this.props.jobs;
    let listItems = [];
    for (var id in jobs) {
      if (jobs.hasOwnProperty(id)) {
        let job = jobs[id];
        if (typeof this.props.filter !== 'undefined') {
          if (this.props.filter(job) !== true) {
            continue;
          }
        }
        listItems.push(<MetapipeJobListItem key={id} job={jobs[id]}/>);
      }
    }

    listItems.sort((a, b) => Number(a.created) > Number(b.created) ? -1 : 1);

    return (
      <div>
        <ListGroup>
          {listItems}
        </ListGroup>
      </div>
    );
  }
});

export default JobList;
