import React from 'react';
import {ListGroup, ListGroupItem, Input, ProgressBar, Radio} from 'react-bootstrap'
import DatasetInfo from '../DatasetInfo';

let DatasetListElement = React.createClass({

  clickHandler(e) {
    this.props.onSelect(this.props.dataset.id);
  },

  render() {
    return (
      <ListGroupItem active={this.props.selected} onClick={this.clickHandler}>
        <DatasetInfo dataset={this.props.dataset}/>
      </ListGroupItem>
    );
  }
});


let DatasetSelector = React.createClass({
  render() {
    let datasetList = this.props.datasets.map((dataset) => {
      return <DatasetListElement key={dataset.id} dataset={dataset} selected={this.props.selected === dataset.id}
                                 onSelect={this.props.onSelect}/>
    });

    return (
      <ListGroup>
        {datasetList}
      </ListGroup>
    );
  }
});

export default DatasetSelector;
