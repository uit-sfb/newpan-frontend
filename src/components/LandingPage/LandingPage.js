// React
import React, { PropTypes } from 'react';
import {Panel, Button, Collapse} from 'react-bootstrap';

// Actions
import AppActions from '../../actions/AppActions.js';

// Components

// API
import ApiUtils from '../../utils/WebApiUtils.js';

// Stores

import Link from '../../utils/Link';
import withStyles from '../../decorators/withStyles';
import styles from './LandingPage.less'

let LandingPage = React.createClass({

  contextTypes: {
    onSetTitle: PropTypes.func.isRequired
  },

  render() {
    this.context.onSetTitle('META-pipe');
    return(
    <div className="LandingPage">
      <div className='LandingPage-container'>
        <h1><img src={require('../../public/images/MMP_logo_23_color.png')} style={{width: '2.5em', height: '3em'}} alt="METAPIPE_LOGO"/> META-pipe 2.0</h1>
        <br/>
        <p style={{fontSize: '1.5em'}}>A pipeline for annotation and analysis of marine metagenomics samples.</p>
        <br/>
        <p style={{fontSize: '1.3em', marginTop: '10px'}}>
          <a className='btn btn-primary btn-lg' href="/metapipe/start" onClick={Link.handleClick}>Run META-pipe »</a> or <a href="/metapipe/results" onClick={Link.handleClick}>Look at results</a>
        </p>
        </div>
    </div>
    );
  }
});

export default withStyles(styles) (LandingPage);
