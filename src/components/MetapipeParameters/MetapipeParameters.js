// React
import React, {PropTypes} from 'react';
import BB, {Panel, Button, Input} from 'react-bootstrap';
import {ListGroup, ListGroupItem, Accordion} from 'react-bootstrap';
import Checkbox from '../Checkbox';
import Radio from '../Radio';
// Actions
import AppActions from '../../actions/AppActions.js';

// Components

// API
import ApiUtils from '../../utils/WebApiUtils.js';

// Stores


let MetapipeParameters = React.createClass({

  getInitialState() {
    return ({
      showDevTools: false,
      checkBoxes: [
        {useBlastUniref50: 'Uniref50 / Blast+'},
        {useInterproScan5: 'Interpro scan5'},
        {usePriam: 'Priam'},
        {useBlastMarRef: 'MarRef / Blast+'}
        ],
      useMapSeq: this.props.params.useMapSeq,
      mapSeqDB: this.props.params.mapSeqDB,
      useKaiju: this.props.params.useKaiju,
      kaijuDB: this.props.params.kaijuDB
    });
  },

  componentWillMount() {
    let renderCheckBoxes = [];
    //Create all the checkboxes
    for (let box in this.state.checkBoxes) {
      if (this.state.checkBoxes.hasOwnProperty(box)) {
        renderCheckBoxes.push(<Checkbox key={box} id={Object.keys(this.state.checkBoxes[box])[0]} label={this.state.checkBoxes[box]} defaultChecked={true}/>)
      }
    }
    this.setState({checkBoxes: renderCheckBoxes})
    //this.props.onChange()
  },

  render() {

    let panelHeader = (header) => {
      return <h2>{header}</h2>
    };
    return (
      <div>
        <div className='form-group' onChange={this.props.onChange}>
          <Panel header={panelHeader("Quality control and assembly")}>
            <label for='inputContigsCutoff'>Cutoff</label>
            <input type='number' className='form-control' id='inputContigsCutoff' min='0' defaultValue='500'/>
            <Checkbox id="removeNonCompleteGenes" label="Remove non-complete genes" defaultChecked={false}/>
            <label for='minContigLength' style={{ marginTop: 10 }}>Minimum contig length</label>
            <input type='number' className='form-control' id='minContigLength' min='0' defaultValue='1000'/>
          </Panel>
          <Panel header={panelHeader('Taxonomic classification')}>
            <Checkbox id="useMapSeq" label="MapSeq" defaultChecked={this.state.useMapSeq} style={{float: 'left', marginRight: 15}}/>
            <Radio id="mapSeqDB" values={['default', 'silvaMar']} labels={['Default (Silva)', 'Silva Mar']} checkedValue={this.state.mapSeqDB} style={{float: 'left', marginRight: 80}}/>
            <Checkbox id="useKaiju" label="Kaiju" defaultChecked={this.state.useKaiju} style={{float: 'left', marginRight: 15}}/>
            <Radio id="kaijuDB" values={['default', 'marDB']} labels={['Default (RefSeq) ', 'MarDB']} checkedValue={this.state.kaijuDB} style={{float: 'left', marginRight: 80}}/>
          </Panel>
          <Panel header={panelHeader('Functional assignment')}>
            {this.state.checkBoxes}
          </Panel>
          <Panel header={panelHeader("Output")}>
            <Checkbox id="exportMergedGenbank" label="Create a Genbank file containing all the contigs and annotations merged together as a single entry" defaultChecked={true}/>
          </Panel>
        </div>
      </div>
    );
  }
});

export default MetapipeParameters;
