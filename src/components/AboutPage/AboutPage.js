/**
 * Created by tteige on 02.06.17.
 */

import React, {PropTypes} from 'react';
import style from './AboutPage.less';
import withStyles from '../../decorators/withStyles';
@withStyles(style)
class AboutPage extends React.Component {

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired
  };

  render () {
    this.context.onSetTitle('META-pipe: About');
    return (
      <div className="container">
        <h1>About META-pipe</h1>
        <p>
          META-pipe is a complete workflow for the analysis of marine metagenomic data. It provides assembly of
          high-throughput sequence data, functional annotation of predicted genes, and taxonomic profiling.
        </p>
      </div>
    );
  }

}

export default AboutPage;
