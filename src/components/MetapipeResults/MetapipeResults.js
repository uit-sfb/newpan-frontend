// React
import React, {PropTypes} from 'react';
import {Panel, Button, Collapse} from 'react-bootstrap';
var Router = require('react-router');
var RouteHandler = Router.RouteHandler;

// Actions
import AppActions from '../../actions/AppActions.js';

// Components
import Result from '../Result';
import ResultsPage from '../ResultsPage';

// API
import ApiUtils from '../../utils/WebApiUtils.js';

// Stores
import JobStore from '../../stores/JobStore.js';
import DatasetStore from '../../stores/DatasetStore.js';


let MetapipeResults = React.createClass({

  contextTypes: {
    onSetTitle: PropTypes.func.isRequired
  },

  getInitialState() {
    return {
      jobs: {},
      datasets: {}
    }
  },

  componentWillMount() {
    JobStore.onChange(this.onJobsUpdate);
    DatasetStore.onChange(this.onDatasetsUpdate);
    this.onJobsUpdate();
    this.onDatasetsUpdate();
  },

  componentWillUnmount() {
    JobStore.off(this.onJobsUpdate);
    DatasetStore.off(this.onDatasetsUpdate);
  },

  onDatasetsUpdate() {
    this.setState({
      datasets: DatasetStore.getDatasets(),
      jobs: this.state.jobs
    });
  },

  onJobsUpdate() {
    let jobs = JobStore.getJobs();
    try {
      let newState = this.state;
      newState.jobs = jobs;
      this.setState(newState);
    } catch (e) {
      console.log('----------------------------------');
      console.log(jobs);
      console.log('++++++++++++++++++++++++++++++++++');
      throw e;
    }
  },

  render() {
    this.context.onSetTitle('META-pipe: Results');
    let jobId = this.props.params.jobId;


    if (typeof jobId === 'undefined') {
      return (
        <ResultsPage jobs={this.state.jobs}/>
      );
    }

    let job = this.state.jobs[this.props.params.jobId];
    if (!job) {
      return (<p>no such job</p>)
    } else {
      return (
        <div>
          <Result job={job} datasets={this.state.datasets}/>
        </div>
      );
    }
  }
});

export default MetapipeResults;
