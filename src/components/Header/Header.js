/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React from 'react';
import styles from './Header.less';
import withStyles from '../../decorators/withStyles';
import Link from '../../utils/Link';
import Navigation from '../Navigation';

@withStyles(styles)
class Header extends React.Component {

  render() {
    return (
      <div className="Header">
        <div className="Header-container">
          <a className="Header-brand" href="/" onClick={Link.handleClick}>
            <img className="Header-brandImg" src={require('../../public/images/MMP_logo_23.png')}
                 alt="METAPIPE_LOGO_WHITE"/>
            <span className="Header-brandTxt">META<text style={{fontFamily: "CORBEL"}}>-</text>pipe</span>
          </a>
          <Navigation className="Header-nav"/>
        </div>
      </div>
    );
  }
}

export default Header;
