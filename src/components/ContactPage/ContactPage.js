/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, {PropTypes} from 'react';
import styles from './ContactPage.less';
import withStyles from '../../decorators/withStyles';

@withStyles(styles)
class ContactPage {

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired
  };

  render() {
    let title = 'META-pipe: Contact Us';
    this.context.onSetTitle(title);
    return (
      <div className="ContactPage">
        <div className="ContactPage-container">
          <h1>{title}</h1>
          <div className='row'>
            <div className='col-md-4'>
              <h3>The META-pipe Team</h3>
              <p>
                Centre for bioinformatics (SfB)<br/>
                Department of Chemistry,<br/>
                Faculty of Science and Technology<br/>
                UiT The Arctic University of Norway<br/>
                9037 Tromsø, Norway
              </p>
            </div>
            <div className='col-md-4'>
              <h3>Contact Information</h3>
              <p>
                E-mail: <a href='mailto:mmp@uit.no'><u>mmp@uit.no</u></a>
                <br/>
                Phone: (+47) 77644651
              </p>
            </div>
            <div className='col-md-4'>
              <h2>Visiting Address</h2>
              <p>
                Siva Innovation Centre<br/>
                Sykehusvegen 21<br/>
                9019 Tromsø, Norway
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default ContactPage;
