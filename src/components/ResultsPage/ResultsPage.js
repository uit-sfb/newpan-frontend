// React
import React, {PropTypes} from 'react';
import {Panel, Button, Collapse} from 'react-bootstrap';

// Actions
import AppActions from '../../actions/AppActions.js';

// Components
import JobList from '../JobList';

// API
import ApiUtils from '../../utils/WebApiUtils.js';

// Stores


let ResultsPage = React.createClass({

  render() {
    let jobFilter = (job) => {
      return job.state === 'FINISHED';
    };

    return (
      <div className='container'>
        <h1>Results</h1>
        <JobList jobs={this.props.jobs} filter={jobFilter}/>
      </div>
    );
  }
});

export default ResultsPage;
