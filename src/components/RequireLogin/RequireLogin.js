import React, { PropTypes } from 'react';
import UserStore from '../../stores/UserStore';

import Login from '../../utils/Login';

var LoggingIn = React.createClass({
  render() {
    return(
      <div>logging in...</div>
    );
  }
});

var RequiresLogIn = React.createClass({
  render() {
    return(
    <div className='container'>
      <div className="jumbotron">
        <h2>Please sign in</h2>
        <p className="lead">
        META-pipe uses the Elixir AAI. You may use it to sign in using your university credentials or your Google account.
        </p>
        <p>
          <a className='btn btn-lg btn-success' href="#" role='button' onClick={Login.clickHandler}>
            <img src='/elixir.png' height='50'/>
            &nbsp;
            Log in with Elixir AAI
          </a>
        </p>
      </div>
    </div>
    );
  }
});

export default React.createClass({
  componentWillMount() {
    UserStore.onChange(this.userStoreUpdate);
    this.userStoreUpdate();
  },

  userStoreUpdate() {
    this.setState(UserStore.getState());
  },

  render() {
    var page = <div><LoggingIn/><RequiresLogIn/></div>;

    if(this.state.user !== null) {
      page = this.props.children;
    } else if(this.state.signingIn === true) {
      page = <LoggingIn/>;
    } else {
      page = <RequiresLogIn/>;
    }

    return(<div>{page}</div>);
  }
});
