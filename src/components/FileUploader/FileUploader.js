import React from 'react';

import {Panel, Button, Input} from 'react-bootstrap';

let FileUploader = React.createClass({
  getInitialState() {
    return {
      name: ''
    }
  },

  submitHandler() {
    this.props.onSubmit(this.state);
  },

  nameChangeHandler() {
    this.setState({
      name: this.refs.name.getValue(),
      file: this.state.file
    });
  },

  fileChangeHandler(e) {
    let file = this.refs.file.getInputDOMNode().files[0];
    let name = this.state.name;
    if (name == '') {
      name = file.name;
    }
    this.setState({
      name: name,
      file: file
    });
  },

  render() {
    let buttonEnabled = this.state.name !== '' && this.state.file !== null;

    return(
      <div>
        <Input type='text' value={this.state.name} ref='name' onChange={this.nameChangeHandler} placeholder='Name of new data set'/>
        <Input type='file' ref='file' onChange={this.fileChangeHandler}/>
        <Button onClick={this.submitHandler} bsStyle='primary' disabled={!buttonEnabled}>Upload</Button>
        <p>{this.state.name}</p>
      </div>
    );
  }
});

export default FileUploader;
