/**
 * Created by tteige on 26.05.17.
 */

import React, {Proptypes} from 'react'

class Radio extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      checkedValue: this.props.checkedValue,
      values: this.props.values,
      labels: this.props.labels
    };
    if(this.props.onClick) {
      this.onClick = this.props.onClick;
    } else {
      this.onClick = this.onHandleClick.bind(this);
    }

  }

  onHandleClick(event) {
    let newState = this.state;
    newState.checkedValue = event.target.value;
    this.setState(newState);
    event.target.value = newState.checkedValue;
  }

  render () {
    var radios = [];
    for (var i = 0; i < this.props.values.length; i++) {
      if (this.state.checkedValue == this.props.values[i]) {
        radios.push(<div className='radio'><label><input type='radio' id={this.props.id} onClick={this.onClick} value={this.props.values[i]} checked={true}/>{this.props.labels[i]}</label></div>);
      }
      else {
        radios.push(<div className='radio'><label><input type='radio' id={this.props.id} onClick={this.onClick} value={this.props.values[i]}/>{this.props.labels[i]}</label></div>);
      }
    }
    return (
      <div style={this.props.style}>
        {radios}
      </div>
    );
  }

};

export default Radio;
