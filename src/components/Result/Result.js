// React
import React, {PropTypes} from 'react';
import {Accordion, Panel, Button, Collapse, ListGroup, ListGroupItem, Table} from 'react-bootstrap';

// Actions

// Components
import DatasetInfo from '../DatasetInfo'

// API
import ApiUtils from '../../utils/WebApiUtils.js';

// Stores
import DatasetStore from '../../stores/DatasetStore.js';


let Result = React.createClass({

  render() {
    let job = this.props.job;
    let debugResultFiles = [];
    let userResultFiles = [];
    for (let key in job.outputUrls) {
      if (job.outputUrls.hasOwnProperty(key)) {
        let baseUrl = job.outputUrls[key].url.split("?")[0];
        let datasets = DatasetStore.getDatasets();
        let dataset = datasets[baseUrl];
        if (typeof dataset === 'undefined') {
          continue; // todo: this is a dirty work-around for a race condition: what if the DatasetStore is out of sync with the JobStore?
        }

        dataset.name = key;

        let isDebugFile = false;
        for (let i in dataset.tags) {
          if (dataset.tags.hasOwnProperty(i)) {
            if (dataset.tags[i] === 'format:j') {
              isDebugFile = true;
            }
          }
        }
        let data = (<ListGroupItem>
          <DatasetInfo dataset={dataset}/>
        </ListGroupItem>);

        if (isDebugFile) {
          debugResultFiles.push(data);
        } else {
          userResultFiles.push(data);
        }
      }
    }

    return (
      <div className='container'>
        <h3>{job.description}</h3>
        <Panel header='Result files'>
          <ListGroup fill='true'>
            {userResultFiles}
          </ListGroup>
        </Panel>
        <Accordion>
          <Panel header='Debug files' eventKey='2'>
            <ListGroup fill='true'>
              {debugResultFiles}
            </ListGroup>
          </Panel>
        </Accordion>

      </div>
    );
  }
});

export default Result;
