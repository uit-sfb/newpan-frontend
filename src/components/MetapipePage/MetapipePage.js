import React, { PropTypes } from 'react';
import {Panel, Nav, NavItem} from 'react-bootstrap'
var Router = require('react-router');
var RouteHandler = Router.RouteHandler;
import Link from '../../utils/Link';
import AppActions from '../../actions/AppActions.js';
import RequireLogin from '../RequireLogin';

export default React.createClass({
  onSelectHandler(_, href) {
    AppActions.navigateTo(href);
  },

  render() {
    return(
    <RequireLogin>
      <div>
        <Nav hidden bsStyle='pills'>
          <NavItem href='/metapipe/start' onSelect={this.onSelectHandler}><b><u>Run META-pipe</u></b></NavItem>
          <NavItem href='/metapipe/jobs' onSelect={this.onSelectHandler}><b><u>Running jobs</u></b></NavItem>
          <NavItem href='/metapipe/results' onSelect={this.onSelectHandler}><b><u>See results</u></b></NavItem>
        </Nav>
        <RouteHandler user={this.props.user}/>
      </div>
    </RequireLogin>
    );
  }
});
