import React, {PropTypes} from 'react';
import styles from './signin.css';
import withStyles from '../../decorators/withStyles';
//import forms from 'newforms';
import Input from 'react-bootstrap/lib/Input';
import ButtonInput from 'react-bootstrap/lib/ButtonInput';

const LoginForm = withStyles(styles)(React.createClass({
  propTypes: {
    onSubmit: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired
  },

  getInitialState() {
    return {
      username: '',
      password: '',
      style: null
    };
  },

  handleOnChange() {
    let username = this.refs.username.getValue();
    let password = this.refs.password.getValue();

    var style = null;
    if ((username.length > 0) && (password.length > 0)) {
      style = 'success';
    }

    this.setState({
      username: username,
      password: password,
      style: style
    });
  },

  handleSubmit(e) {
    e.preventDefault();
    console.log(this.state);
    this.props.onSubmit(this.state.username, this.state.password);
  },

  render() {
    let user = JSON.stringify(this.props.user);

    return (
      <form className='form-signin'>
        <Input type="text" ref="username" onChange={this.handleOnChange} placeholder='Username'/>
        <Input type="password" ref="password" onChange={this.handleOnChange} placeholder='Password'/>
        <ButtonInput type='submit' value='Sign in' bsStyle={this.state.style} onClick={this.handleSubmit}/>
        <p>username: {this.state.username}</p>
        <p>user: {this.state.username.length}, pass: {this.state.password.length}</p>
        <p>user-props: {user}</p>
      </form>
    );
  }

}));

export default LoginForm;
