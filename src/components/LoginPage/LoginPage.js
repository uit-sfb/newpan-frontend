/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes } from 'react';
import styles from './LoginPage.less';
import withStyles from '../../decorators/withStyles';
import LoginForm from '../LoginForm';
import UserActions from '../../actions/UserActions'

@withStyles(styles)
class LoginPage {

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired
  };

  handleSignIn(username, password) {
    UserActions.signIn(username, password); // todo callback success failure
  };

  render() {
    let title = 'Log In';
    this.context.onSetTitle(title);
    return (
      <div className="LoginPage">
        <div className="LoginPage-container">
          Please wait...
        </div>
      </div>
    );
  }

}

export default LoginPage;
