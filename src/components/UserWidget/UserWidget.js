import React, { PropTypes } from 'react';
import UserStore from '../../stores/UserStore';
import Login from '../../utils/Login';
import {Panel, Nav, NavItem, NavDropdown, NavBar} from 'react-bootstrap'
import UserActions from '../../actions/UserActions.js';

var UserMenu = React.createClass({
  render() {
    var user = this.props.user;
    //<div className="Navigation-link Navigation-link--highlight">{user.name}</div>

    var logoutClickHandler = function() {
      UserActions.logout();
    };

    return(
      <div className="dropdown Navigation-link Navigation-link--highlight">
        <a href="#" className="dropdown-toggle dropdown btn Navigation-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          {user.name} <span className="caret"></span>
        </a>
        <ul className="dropdown-menu" aria-labelledby="dLabel">
          <li><a href="#" onClick={logoutClickHandler}>
          <div className='glyphicon glyphicon-off'/>&nbsp;Log out</a></li>
        </ul>
      </div>
    );
  }
});

// Old: <a className="Navigation-link" href="/login" onClick={Login.clickHandler}>Log in</a>
var LoginButton = React.createClass({
  render() {
    return(
      <div className="dropdown Navigation-link Navigation-link--highlight">
        <a href="/login" onClick={Login.clickHandler} className="dropdown-toggle dropdown btn Navigation-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          Log in
        </a>
      </div>
    );
  }
});

export default React.createClass({
  componentWillMount() {
    UserStore.onChange(this.userStoreUpdate);
    this.userStoreUpdate();
  },
  componentWillUnmount() {
    UserStore.off(this.userStoreUpdate);
  },

  userStoreUpdate() {
    this.setState(UserStore.getState());
  },

  render() {
    var widget;
    if(this.state.user === null) {
      widget = <LoginButton/>;
    } else {
      widget = <UserMenu user={this.state.user}/>
    }

    //return(<NavDropdown eventKey={3} title="Dropdown" id="basic-nav-dropdown"></NavDropdown>);

    return(widget);
  }
});
