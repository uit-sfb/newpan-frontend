/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes } from 'react';
import styles from './Footer.less';
import withViewport from '../../decorators/withViewport';
import withStyles from '../../decorators/withStyles';
import Link from '../../utils/Link';

@withViewport
@withStyles(styles)
class Footer extends React.Component {

  static propTypes = {
    viewport: PropTypes.shape({
      width: PropTypes.number.isRequired,
      height: PropTypes.number.isRequired
    }).isRequired
  };

  render() {
    // This is just an example how one can render CSS
    let { width, height } = this.props.viewport;
    this.renderCss(`.Footer-viewport:after {content:' ${width}x${height}';}`);

    return (
      <div className="Footer">
        <div className="Footer-container">
          <a href="http://www.elixir-norway.org" target="_blank">
            <img className="Footer-img-left" src={require('../../public/images/elixirNorway.png')} alt="Elixir Norway"/>
          </a>
          <a className="Footer-link" href="https://www.elixir-europe.org/services/tools/meta-pipe" target="_blank">META-pipe is part of the ELIXIR infrastructure</a>
          <span className="Footer-spacer"> | </span>
          <span className="Footer-text">© 2018 SfB - UiT</span>
          <span className="Footer-spacer"> | </span>
          <a hidden className="Footer-link" href="/" onClick={Link.handleClick}>Home</a>
          <span hidden className="Footer-spacer"> | </span>
          <a hidden className="Footer-link" href="/privacy" onClick={Link.handleClick}>Privacy</a>
          <span hidden className="Footer-spacer"> | </span>
          <a className="Footer-link" href="https://mmp.sfb.uit.no/terms" target="_blank">Terms and conditions</a>
          <span hidden className="Footer-spacer"> | </span>
          <span hidden ref="viewport" className="Footer-viewport Footer-text Footer-text--muted">Viewport:</span>
          <a href="http://elixir-europe.org/excelerate" target="_blank">
            <img className="Footer-img-right" src={require('../../public/images/excelerateEU.png')} alt="Elixir Europe Excelerate"/>
          </a>
        </div>
      </div>
    );
  }

}

export default Footer;
