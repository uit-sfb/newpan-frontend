// React
import React, { PropTypes } from 'react';
import {Panel, Button, Collapse} from 'react-bootstrap';

// Actions
import AppActions from '../../actions/AppActions.js';

// Components

// API
import ApiUtils from '../../utils/WebApiUtils.js';

// Stores



let DownloadButton = React.createClass({
  render() {
    let downloadDataset = function() {
      let url = ApiUtils.createDownloadLink(this.props.dataset.id);
      console.log("Navigate to: " + url);
      window.location.href = url;
    }.bind(this);

    return(
      <Button bsStyle="success" onClick={downloadDataset}><div style={{fontSize: 1.5+'em'}} className="glyphicon glyphicon-download"></div></Button>
    );
  }
});

export default DownloadButton;
