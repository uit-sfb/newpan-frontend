/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes } from 'react';
import classNames from 'classnames';
import styles from './Navigation.less';
import withStyles from '../../decorators/withStyles';
import Link from '../../utils/Link';
import Login from '../../utils/Login';
import UserWidget from '../UserWidget';

@withStyles(styles)
class Navigation {

  static propTypes = {
    className: PropTypes.string
  };

  render() {
    return (
      <div className={classNames(this.props.className, 'Navigation')} role="navigation">
        <a className="Navigation-link" href="/metapipe/start" onClick={Link.handleClick}><b>Run META-pipe</b></a>
        &nbsp; &nbsp;
        <a className="Navigation-link" href="/metapipe/jobs" onClick={Link.handleClick}><b>Running Jobs</b></a>
        &nbsp; &nbsp;
        <a className="Navigation-link" href="/metapipe/results" onClick={Link.handleClick}><b>See Results</b></a>
        &nbsp; <span className="Navigation-spacer"> | </span> &nbsp;
        <a className="Navigation-link" href="https://mmp.sfb.uit.no/documentation/#/?id=meta-pipe" target="_blank"><b>DOCUMENTATION</b></a>
        &nbsp; &nbsp;
        <a className="Navigation-link" href="https://mmp.sfb.uit.no/contact/" target="_blank"><b>CONTACT</b></a>
        &nbsp; <span className="Navigation-spacer"> | </span> &nbsp;
        <UserWidget/>
      </div>
    );
  }
}

export default Navigation;
