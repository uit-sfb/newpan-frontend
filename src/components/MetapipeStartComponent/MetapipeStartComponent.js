import JobActions from '../../actions/JobActions.js';
import React, {PropTypes} from 'react';
import DatasetSelector from '../DatasetSelector';

import {Panel, Button, Collapse, ButtonToolbar, ToggleButton, ToggleButtonGroup} from 'react-bootstrap';
import {ListGroup, ListGroupItem, Input, ProgressBar} from 'react-bootstrap'

import withStyles from '../../decorators/withStyles';
import styles from './MetapipeStart.less';
import DatasetStore from '../../stores/DatasetStore.js';
import DatasetActions from '../../actions/DatasetActions.js';
import FileUploader from '../FileUploader';
import DatasetInfo from '../DatasetInfo';
import ApiUtils from '../../utils/WebApiUtils.js';
import AppActions from '../../actions/AppActions.js';
import MetapipeParameters from '../MetapipeParameters';
import Checkbox from '../Checkbox'



let defaultDatasetType = 'FASTA';

let DatasetTypeForm = React.createClass({
  getInitialState() {
    return {
      selectedDatasetType: defaultDatasetType,
      readsEnabled: false
    };
  },

  render() {
    let selectReads = () => {
      this.setState({
        selectedDatasetType: 'READS'
      });
      this.props.onSelectDatasetType('READS')
    };

    let selectFasta = () => {
      this.setState({
        selectedDatasetType: 'FASTA'
      });
      this.props.onSelectDatasetType('FASTA')
    };

    let readsButtonClass = "btn btn-default"
    let fastaButtonClass = "btn btn-default"
    if(this.state.selectedDatasetType == 'READS') {
      readsButtonClass += " active"
    }
    else if(this.state.selectedDatasetType == 'FASTA') {
      fastaButtonClass += " active"
    }

    return (
      <div className='container'>
        <div className='row' style={{textAlign: 'center'}}>
          <div class="btn-group" data-toggle="buttons" bsSize="large">
            <label className={readsButtonClass} onClick={selectReads} disabled={!this.state.readsEnabled} style={{fontSize: 2 + 'em', width: 8 + 'em', marginRight: 4 + 'em'}}>
              <input type="radio" name="options" id="option1" autocomplete="off" checked={this.state.selectedDatasetType == 'READS'}/>
              Reads
            </label>
            <label className={fastaButtonClass} onClick={selectFasta} style={{fontSize: 2 + 'em', width: 8 + 'em'}}>
              <input type="radio" name="options" id="option2" autocomplete="off" checked={this.state.selectedDatasetType == 'FASTA'}/>
              Fasta
            </label>
          </div>
        </div>
      </div>
    );
  }
});

let DatasetForm = React.createClass({
  getInitialState() {
    return {
      method: 'NOT_SELECTED'
    };
  },

  setInitialState() {
    this.setState({
      method: 'NOT_SELECTED'
    });
  },

  render() {
    let selectUploadHandler = () => {
      this.setState({
        method: 'UPLOAD'
      });
    };

    let selectChooseExistingHandler = () => {
      this.setState({
        method: 'EXISTING'
      });
    };

    let backHandler = () => {
      this.setState({
        method: 'NOT_SELECTED'
      });
    };

    let onDatasetUpload = (upload) => {
      console.log(upload);
    };

    let onChooseDifferent = () => {
      this.props.onSelect('');
      this.setState({
        method: 'NOT_SELECTED'
      });
    };

     console.log('this.state.method: "' + this.state.method + '"');

    let textColClass, buttonColClass, dialoguesColClass, containerStyle, text;
    if (this.props.selectedDatasetType == 'READS') {
      textColClass='col-md-2';
      buttonColClass='col-md-4';
      dialoguesColClass='col-md-12';
      if (this.props.datasetFormNumber == '1') {
        containerStyle = {
            float: 'left',
            width: '49%',
        };
      }
      else if (this.props.datasetFormNumber == '2') {
        containerStyle = {
            float: 'right',
            width: '49%',
        };
      }
      text='R' + this.props.datasetFormNumber + ':';
    }
    else if (this.props.selectedDatasetType == 'FASTA') {
      textColClass='col-md-2';
      buttonColClass='col-md-2';
      dialoguesColClass='col-md-12';
      if (this.props.datasetFormNumber == '1') {
        containerStyle = {
            width: '100%',
        };
      }
      else if (this.props.datasetFormNumber == '2') {
        return (null);
      }
      text='Fasta:';
    }

    let goBack = <p><a onClick={backHandler} href='#'>Go back</a></p>;
    if (this.props.selected === '') {
      switch (this.state.method) {
        case 'NOT_SELECTED':
          return (
            <div className='container' style={containerStyle}>
              <div className='row'>
                <div className={textColClass}>
                  <h2 style={{marginTop: 1 + 'em'}}>{text}</h2>
                </div>
                <div className={buttonColClass}>
                  <a className='btn btn-default btn-lg' onClick={selectUploadHandler}>
                    <div className='glyphicon glyphicon-upload' style={{fontSize: 3 + 'em'}}/>
                    &nbsp;
                    <div>Upload new</div>
                  </a>
                </div>
                <div className={buttonColClass}>
                  <a className='btn btn-default btn-lg' onClick={selectChooseExistingHandler} href='#'>
                    <div className='glyphicon glyphicon-list-alt' style={{fontSize: 3 + 'em'}}/>
                    &nbsp;
                    <div>Choose existing</div>
                  </a>
                </div>
              </div>
            </div>
          );
          break;
        case 'UPLOAD':
          return (
            <div className='container' style={containerStyle}>
              <div className='row'>
                <div className={textColClass}>
                  <h2 style={{marginTop: 1 + 'em'}}>{text}</h2>
                </div>
                <div className={dialoguesColClass}>
                  {goBack}
                  <FileUploader onSubmit={this.props.onUpload}/>
                </div>
              </div>
            </div>
          );
          break;
        case 'EXISTING':
          return (
            <div className='container' style={containerStyle}>
              <div className='row'>
                <div className={textColClass}>
                  <h2 style={{marginTop: 1 + 'em'}}>{text}</h2>
                </div>
                <div className={dialoguesColClass}>
                  {goBack}
                  <DatasetSelector datasets={this.props.datasets} selected={this.props.selected}
                             onSelect={this.props.onSelect}/>
                </div>
              </div>
            </div>
          );
          break;
        case 'SELECTED':
          return (
            <div className='container' style={containerStyle}>
              <div className='row'>
                <div className={textColClass}>
                  <h2 style={{marginTop: 1 + 'em'}}>{text}</h2>
                </div>
                <div className={dialoguesColClass}>
                  Dataset selected
                  {goBack}
                </div>
              </div>
            </div>
          );
          break;
      }
    } else {
      let dataset;
      for (let i in this.props.datasets) {
        if (this.props.datasets.hasOwnProperty(i)) {
          let cur = this.props.datasets[i];
          if (cur.id === this.props.selected) {
            dataset = cur;
          }
        }
      }
      return (
        <div className='container' style={containerStyle}>
          <div className='row'>
            <div className={textColClass}>
              <h2 style={{marginTop: 1 + 'em'}}>{text}</h2>
            </div>
            <div className={dialoguesColClass}>
              <a href='#' onClick={onChooseDifferent}>Choose a different dataset</a>
              <ListGroup>
                <ListGroupItem>
                  <DatasetInfo dataset={dataset}/>
                </ListGroupItem>
              </ListGroup>
            </div>
          </div>
        </div>
      );
    }
    return (<div/>);
  }
});

let MetapipeStart = React.createClass({

  contextTypes: {
    onSetTitle: PropTypes.func.isRequired
  },

  getInitialState() {
    this.onSelectDatasetType = this.onSelectDatasetType.bind(this)
    return {
      selectedDatasetType: defaultDatasetType,
      selectedDatasetId1: '',
      selectedDatasetId2: '',
      datasets: [],
      submitting: false,
      jobId: ApiUtils.generateId(),
      tag: 'metapipe',
      parameters: {
        inputContigsCutoff: 500,
        useBlastUniref50: true,
        useInterproScan5: true,
        usePriam: true,
        removeNonCompleteGenes: false,
        exportMergedGenbank: true,
        useBlastMarRef: true,
        useMapSeq: true,
        mapSeqDB: 'default',
        useKaiju: false,
        kaijuDB: 'default',
        minContigLength: 1000
      },
      error_strings: {},
      enableDevTools: false
    };
  },

  //todo: add output datasets to state

  componentWillMount() {
    DatasetStore.onChange(this.onDatasetUpdate);
    this.onDatasetUpdate();
  },

  componentWillUnmount() {
    DatasetStore.off(this.onDatasetUpdate);
  },

  onSelectDatasetType(newSelectedDatasetType) {
    this.getInitialState();
    this.setState({
      selectedDatasetType: newSelectedDatasetType,
      selectedDatasetId1: '',
      selectedDatasetId2: ''
    });
    this.refs.datasetForm1.setInitialState();
    this.refs.datasetForm2.setInitialState();
  },

  onDatasetUpdate() {
    let newState = this.state;
    newState.datasets = [];

    let storedDatasets = DatasetStore.getDatasets();

    // console.log(storedDatasets);

    for (let key in storedDatasets) {
      if (storedDatasets.hasOwnProperty(key)) {
        let dataset = storedDatasets[key];
        if (dataset.tags.indexOf("format_fasta") !== -1) {
          newState.datasets.push(dataset);
        }
      }
    }
    newState.datasets.sort((a, b) => a.created > b.created ? -1 : 1);

    this.errorCheck(newState);
  },

  onDataset1Upload(uploaderState) {
//    let format;
//    if (this.state.selectedDatasetType == 'READS') {
//        format = 'format_reads';
//    }
//    else if (this.state.selectedDatasetType == 'FASTA') {
//        format = 'format_fasta';
//    }
    let format = 'format_fasta';
    let url = DatasetActions.upload(uploaderState.name, uploaderState.file, [format]);
    this.onSelectDataset1(url);
  },
  onDataset2Upload(uploaderState) {
//    let format;
//    if (this.state.selectedDatasetType == 'READS') {
//        format = 'format_reads';
//    }
//    else if (this.state.selectedDatasetType == 'FASTA') {
//        format = 'format_fasta';
//    }
    let format = 'format_fasta';
    let url = DatasetActions.upload(uploaderState.name, uploaderState.file, [format]);
    this.onSelectDataset2(url);
  },

  onSelectDataset1(datasetId) {
    let newState = this.state;
    newState.selectedDatasetId1 = datasetId;
    this.errorCheck(newState)
  },
  onSelectDataset2(datasetId) {
    let newState = this.state;
    newState.selectedDatasetId2 = datasetId;
    this.errorCheck(newState)
  },

  submitHandler(event) {
    let state = this.state;
    state.submitting = true;
    this.setState(state);

    event.preventDefault();
    let job = {
      selectedDatasetType: this.state.selectedDatasetType,
      inputDataset1: this.state.selectedDatasetId1,
      inputDataset2: this.state.selectedDatasetId2,
      id: this.state.jobId,
      parameters: this.state.parameters,
      tag: this.state.tag
    };

    // todo: validate job. Unable to submit an invalid job, might be better to verify the job here though.
    JobActions.submitJob(job)
      .then(p => JobActions.refreshJobs())
      .then(p => AppActions.navigateTo('/metapipe/jobs'));
  },

  refreshHandler(event) {
    event.preventDefault();
    DatasetActions.refreshDatasets();
  },

  //Checks for errors in state, done like this in order to support more errors. Currently only no dataset selected
  //To add more errors, create a new if statement for the desired state
  errorCheck(state) {
    let newState = state;
    if (state.selectedDatasetId1 !== '') {
      delete newState.error_strings['dataset'];
    } else {
      newState.error_strings['dataset'] = 'No dataset selected. Please select a dataset in order to continue'
    }
    if (state.parameters['inputContigsCutoff'] > 0) {
      delete newState.error_strings['inputContigsCutoff'];
    } else {
      newState.error_strings['inputContigsCutoff'] = 'Invalid cutoff value, must be greater than 0'
    }
    if (state.parameters['minContigLength'] > 0) {
      delete newState.error_strings['minContigLength'];
    } else {
      newState.error_strings['minContigLength'] = 'Invalid minimum contig length value, must be greater than 0'
    }
    if (state.tag === '') {
      newState.error_strings['tag'] = 'No executor tag specified'
    } else {
      delete newState.error_strings['tag'];
    }

    this.setState(newState);
    return Object.keys(this.state.error_strings).length > 0;
  },

  //Sets the parameters for the job
  //If more params are added, the id of the parameter component has to match the id of the parameter name
  setParams(event) {
    let newState = this.state;
    //Has to do a check here because the value was converted to string for the checkboxes,
    // which caused the JSON.stringify to parse the parameter values differently if the checkbox was never toggled.
    if (event.target.type === 'checkbox') {
      //Just convert the value to explicitly to boolean value
      //Converting by using newState.parameters[event.target.id] = (event.target.value === 'true') does not work here?
      if (event.target.id in this.state.parameters) {
        if (event.target.value === 'true') {
          newState.parameters[event.target.id] = true;
        } else {
          newState.parameters[event.target.id] = false;
        }
      }
    } else if (event.target.type === 'radio') {
      newState.parameters[event.target.id] = event.target.value;
    } else if (event.target.id in newState.parameters) {
      newState.parameters[event.target.id] = Number(event.target.value);
    } else {
      newState[event.target.id] = event.target.value;
    }
    this.errorCheck(newState);
    console.log(newState);
  },


  showDevTools(e) {
    let newState = this.state;
    newState.enableDevTools = e.target.checked;
    this.setState(newState);
  },

  getDevTools() {
	  let devTools = <div/>;
	  if (this.state.enableDevTools) {
		  devTools = (
			  <Panel header='Dev-Tools'>
				  <label class='sr-only' for='tag'>Job tag</label>
				  <Input type='text' id='tag' placeholder='metapipe' onChange={this.setParams}/>
				  <label class='sr-only' for='jsonParams'>Current parameters</label>
				  <pre id='jsonParams'>{JSON.stringify(JobActions.convertParamsToMetapipeParams(this.state.parameters), null, 2)}</pre>
			  </Panel>
		  );
	  }
	  return devTools;
  },

  render() {
    /*
     <FileUploader onSubmit={this.onDatasetUpload}/>
     <DatasetSelector datasets={this.state.datasets} selected={this.state.selectedDatasetId} onSelect={this.onSelectDataset}/>
     */
    this.context.onSetTitle('META-pipe: Start');
    let panelHeader = (header) => {
      return <h2>{header}</h2>
    };

    //Done in order to support more errors except for "no dataset selected", currently over complex for
    // handling just a single error
    let parameter_errors = [];
    for (let i in this.state.error_strings) {
      if (this.state.error_strings.hasOwnProperty(i)) {
        parameter_errors.push(<ListGroupItem key={i} bsStyle='danger'>{this.state.error_strings[i]}</ListGroupItem>)
      }
    }
    let datasetHeader, datasetMethod;
    if (this.state.selectedDatasetType == 'READS') {
        datasetHeader = 'Select Reads datasets';
    }
    else if (this.state.selectedDatasetType == 'FASTA') {
        datasetHeader = 'Select Fasta dataset';
    }
    return (
      <div className='container'>
        <form>
          <h1>Start Meta-pipe</h1>
          <Panel header={panelHeader('Dataset type')}>
            <DatasetTypeForm onSelectDatasetType={this.onSelectDatasetType}/>
          </Panel>
          <Panel header={panelHeader(datasetHeader)}>
            <DatasetForm ref='datasetForm1' datasets={this.state.datasets} selected={this.state.selectedDatasetId1} selectedDatasetType={this.state.selectedDatasetType} datasetFormNumber='1'
                    onSelect={this.onSelectDataset1} onUpload={this.onDataset1Upload}/>
            <DatasetForm ref='datasetForm2' datasets={this.state.datasets} selected={this.state.selectedDatasetId2} selectedDatasetType={this.state.selectedDatasetType} datasetFormNumber='2'
                    onSelect={this.onSelectDataset2} onUpload={this.onDataset2Upload}/>
          </Panel>
          <div>
            <ListGroup>
              {parameter_errors}
            </ListGroup>
          </div>
          <Panel header={panelHeader('Select parameters')}>
            <MetapipeParameters onChange={this.setParams} params={this.state.parameters}/>
          </Panel>
          <Button disabled={this.state.submitting || Object.keys(this.state.error_strings).length > 0}
                  onClick={this.submitHandler} type='submit' bsStyle='primary' bsSize='large'>Start Meta-pipe</Button>
        </form>
        <Checkbox id="enableDevTools" label="Enable dev tools" defaultChecked={false} onClick={this.showDevTools}/>
        {this.getDevTools()}
      </div>
    );
  }
});

export default withStyles(styles)(MetapipeStart);
