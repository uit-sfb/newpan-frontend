import React, {PropTypes} from 'react';
import withStyles from '../../decorators/withStyles';
import JobStore from '../../stores/JobStore.js';
import {Panel, Button, ListGroup, ListGroupItem} from 'react-bootstrap'
import JobActions from '../../actions/JobActions.js';
import AppActions from '../../actions/AppActions.js';
import JobList from '../JobList';

let MetapipeJobs = React.createClass({

  contextTypes: {
    onSetTitle: PropTypes.func.isRequired
  },

  getInitialState() {
    return {
      jobs: {}
    }
  },

  componentWillMount() {
    JobStore.onChange(this.onJobsUpdate);
    this.onJobsUpdate();
  },

  componentWillUnmount() {
    JobStore.off(this.onJobsUpdate);
  },

  onJobsUpdate() {
    this.setState({
      jobs: JobStore.getJobs()
    });
  },

  render() {
    let jobs = this.state.jobs;
    let filter = (job) => {
      return (job.state === 'SUBMITTED') || (job.state === 'DELAYED');
    };
    this.context.onSetTitle('META-pipe: Running Jobs');
    return (
      <div className='container'>
        <h1>Running jobs</h1>
        <JobList jobs={jobs} filter={filter}/>
      </div>
    );
  }
});

export default MetapipeJobs;
