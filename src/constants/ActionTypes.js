/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import keyMirror from 'react/lib/keyMirror';

export default keyMirror({
  GET_PAGE: null,
  RECEIVE_PAGE: null,
  CHANGE_LOCATION: null,

  // Authentication
  SIGNING_IN: null,
  SIGNED_IN: null,
  SIGNED_OUT: null,
  SIGN_IN_FAILED: null,

  // Jobs
  JOB_SUBMITTED: null,
  JOB_STARTED: null,
  JOBS_REFRESH: null,

  // Datasets
  DATASET_REFRESH: null,
  UPLOAD_PROGRESS: null
});
