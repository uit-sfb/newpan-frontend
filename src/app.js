/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import 'babel/polyfill';
import React from 'react';
import FastClick from 'fastclick';
import App from './components/App';
import Dispatcher from './core/Dispatcher';
import AppActions from './actions/AppActions';
import ActionTypes from './constants/ActionTypes';
import Router from 'react-router';
import ApiUtils from './utils/WebApiUtils.js'
import UserActions from './actions/UserActions.js'

import routes, {RouterProvider} from './router.js';


let path = decodeURI(window.location.pathname);
let onSetMeta = (name, content) => {
  // Remove and create a new <meta /> tag in order to make it work
  // with bookmarks in Safari
  let elements = document.getElementsByTagName('meta');
  [].slice.call(elements).forEach((element) => {
    if (element.getAttribute('name') === name) {
      element.parentNode.removeChild(element);
    }
  });
  let meta = document.createElement('meta');
  meta.setAttribute('name', name);
  meta.setAttribute('content', content);
  document.getElementsByTagName('head')[0].appendChild(meta);
};

function run() {
  ApiUtils.backFromDead((user) => {
    UserActions.signInSuccess(user);
    AppActions.refresh();

    window.setInterval(AppActions.refresh, 10000);
  });

  // Render the top-level React component
  let props = {
    path: path,
    context: {
      onSetTitle: value => document.title = value,
      onSetMeta
    }
  };

  let router = Router.create({
    routes: routes(props),
    location: Router.HistoryLocation
  });
  RouterProvider.set(router);
  router.run((Root) => {
    React.render(<Root/>, document.getElementById('app'));
  });
}

// Run the application when both DOM is ready
// and page content is loaded
Promise.all([
  new Promise((resolve) => {
    if (window.addEventListener) {
      window.addEventListener('DOMContentLoaded', resolve);
    } else {
      window.attachEvent('onload', resolve);
    }
  }).then(() => FastClick.attach(document.body)),
  //new Promise((resolve) => AppActions.loadPage(path, resolve))
]).then(run);
