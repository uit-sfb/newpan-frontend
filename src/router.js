import React from 'react';
import Router, {Route} from 'react-router';
var RouteHandler = Router.RouteHandler;

// Pages
import App from './components/App'
import MetapipePage from './components/MetapipePage';
import ContentPage from './components/ContentPage';
import ContactPage from './components/ContactPage';
import LoginPage from './components/LoginPage';
import RegisterPage from './components/RegisterPage';
import NotFoundPage from './components/NotFoundPage';
import LandingPage from './components/LandingPage';
import AboutPage from './components/AboutPage'

// Components
import MetapipeStartComponent from './components/MetapipeStartComponent';
import MetapipeJobs from './components/MetapipeJobs';
import MetapipeResults from './components/MetapipeResults';
import Result from './components/Result';

////// For route debugging

var Container = React.createClass({

  render() {
    return (
      <div>
        <h1>hello</h1>
        <RouteHandler {...this.props}/>
      </div>
    );
  }
});

var Props = React.createClass({
  render() {
    this.props.context.onSetTitle('hello props');
    return (
      <div>
        <h2>Props</h2>
        <p>{JSON.stringify(this.props)}</p>
      </div>
    );
  }
});

var Inbox = React.createClass({
  render() {
    return (
      <div>
        <h1>Inbox</h1>
        <RouteHandler/>
      </div>
    );
  }
});

var Email = React.createClass({
  render() {
    return (
      <div>
        <h1>Mail: {this.props.params.id}</h1>
      </div>
    );
  }
});

var HelloWorld = React.createClass({
  render() {
    return (
      <div>
        <h1>Hello World</h1>
      </div>
    );
  }
});

//////

// Taken from: http://stackoverflow.com/questions/27864720/react-router-pass-props-to-handler-component
var withProps = function(Component, props) {
  return React.createClass({
    render: function() {
      return React.createElement(Component, props);
    }
  });
};

var _router = null;

export const RouterProvider = {
  get: () => {
    return _router;
  },
  set: (r) => {
    _router = r;
  }
};

export default (props) => {
  var routes = (
    <Route handler={withProps(App, props)}>
      <Route path="" handler={LandingPage}/>
      <Route path="login" handler={LoginPage}/>
      <Route path='metapipe' handler={MetapipePage}>
        <Route path='start' handler={MetapipeStartComponent}/>
        <Route path='jobs' handler={MetapipeJobs}/>
        <Route path='results' handler={MetapipeResults}>
            <Route path=':jobId' handler={MetapipeResults} />
        </Route>
      </Route>
      <Route path='props' handler={Props} />
      <Route path='contact' handler={ContactPage} />
      <Route path='register' handler={RegisterPage}/>
      <Route path='about' handler={AboutPage}/>
      <Route path="inbox" handler={Inbox}>
        <Route path="messages/:id" handler={Email} />
      </Route>
    </Route>
  );

  /*
  var router = Router.create({
    routes: routes,
    location: Router.HistoryLocation
  });*/

  return (routes);
}

/*
export default {
  router: router,

  run(body) {
    router.run((Root) => {
      React.render(<Root/>, body);
    });
  }
}
*/
