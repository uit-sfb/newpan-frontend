import http from 'superagent';
import Dispatcher from '../core/Dispatcher';
import ActionTypes from '../constants/ActionTypes';
import Api from '../utils/WebApiUtils.js';

export default {
  logInSaml() {
    Dispatcher.dispatch({
      type: ActionTypes.SIGNING_IN
    });

    Api.login();
  },

  signIn(username, password, onSuccess, onFailure) {
    Dispatcher.dispatch({
      type: ActionTypes.SIGNING_IN
    });

    Api.signIn(username, password)
      .then((resp) => {
        console.log('---------');
        let info = resp.body;
        this.signInSuccess(info);
        onSuccess(info);
      }).catch((err) => {
        this.signInFailed('Unknown reason...');
      });
  },

  signInFailed(reason) {
    Dispatcher.dispatch({
      type: ActionTypes.SIGN_IN_FAILED,
      reason: reason
    });
  },

  signInSuccess(user) {
    Dispatcher.dispatch({
      type: ActionTypes.SIGNED_IN,
      user: user
    });
  },

  logout() {
    Api.logout();
    Dispatcher.dispatch({
      type: ActionTypes.SIGNED_OUT
    });
  }
}
