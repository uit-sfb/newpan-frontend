/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import http from 'superagent';
import { canUseDOM } from 'react/lib/ExecutionEnvironment';
import Dispatcher from '../core/Dispatcher';
import ActionTypes from '../constants/ActionTypes';
import {Navigation} from 'react-router';
import {RouterProvider} from '../router.js';
import DatasetActions from './DatasetActions.js';
import JobActions from './JobActions.js';

export default {

  navigateTo(path, options) {
    this.loadPage(path, () => {
      RouterProvider.get().transitionTo(path);
    });
  },

  loadPage(path, cb) {
    if (cb)  {
      console.log(cb);
      cb();
    }
  },

  refresh() {
    return Promise.all([
      DatasetActions.refreshDatasets(),
      JobActions.refreshJobs()
    ]);
  },

  displayMetaPipe() {

  },
};
