import ActionTypes from '../constants/ActionTypes';
import Dispatcher from '../core/Dispatcher';
import UserStore from '../stores/UserStore.js'
import ApiUtils from '../utils/WebApiUtils.js'

export default {
  refreshDatasets() {
    let sub = UserStore.getState().user.sub;
    return ApiUtils.getDatasets(sub).then( (datasets) => {
      Dispatcher.dispatch({
        type: ActionTypes.DATASET_REFRESH,
        datasets: datasets
      });
    });
  },

  upload(name, file, tags) {
    let sub = UserStore.getState().user.sub;
    let url = ApiUtils.generateUploadUrl(sub, name, tags);
    let id = url.split('?')[0];

    let createdAt = 'Å';

    ApiUtils.getUploadToken(name, url).then((token) => {
      let incoming = ApiUtils.incomingClient();
      console.log(token, file);
      let uploader = incoming.Uploader(token, file);

      uploader.onprogress = (up) => {
        Dispatcher.dispatch({
          type: ActionTypes.UPLOAD_PROGRESS,
          dataset: {
            id: id,
            name: name,
            created: createdAt,
            tags: tags,
            upload: {
              progress: up.frac_complete,
              description: up.state_msg,
              tags: tags
            }
          }
        });
      };

      uploader.onfinished = (up) => {
        this.refreshDatasets();
      };

      Dispatcher.dispatch({
        type: ActionTypes.UPLOAD_PROGRESS,
        dataset: {
          id: id,
          name: name,
          created: createdAt,
          tags: tags,
          upload: {
            progress: 0,
            description: ''
          }
        }
      });

      uploader.start();
    });
    return url.split('?')[0];
  }
};
