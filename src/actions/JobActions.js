import ActionTypes from '../constants/ActionTypes';
import Dispatcher from '../core/Dispatcher';
import WebApiUtils from '../utils/WebApiUtils.js';
import UserStore from '../stores/UserStore.js';

export default {

  convertParamsToMetapipeParams(params) {
    let metapipeParams = {...params};
    return metapipeParams;
  },

  submitJob(job, callback) {
    console.log('submitting job');
    console.log(job);

    let sub = UserStore.getState().user.sub;

    let metapipeParams = this.convertParamsToMetapipeParams(job.parameters);

    let data;
    if (job.selectedDatasetType == 'READS') {
      data = {
        inputs: {
           "r1.fastq.gz": {
             url: job.inputDataset1
           },
           "r2.fastq.gz": {
             url: job.inputDataset2
           }
        },
        properties: { },
        tag: job.tag,
        parameters: metapipeParams
      };
    }
    else if (job.selectedDatasetType == 'FASTA') {
      data = {
        inputs: {
          "input.fas": {
            url: job.inputDataset1
          }
        },
        properties: { },
        tag: job.tag,
        parameters: metapipeParams
      };
    }

    console.log(data)

    let promise = WebApiUtils.submitJob(sub, job.id, data).then( (res) => {
      console.log("ok-job")
    });

    // get job id
    // action: submitting
    // submit()
    // onComplete:
    //    action: refresh jobs

    // on job submitted, callback()
    if(callback) {
      callback()
    }
    return promise;
  },

  refreshJobs() {
    let sub = UserStore.getState().user.sub;
    return WebApiUtils.getJobs(sub).then((jobs) => {
      Dispatcher.dispatch({
        type: ActionTypes.JOBS_REFRESH,
        jobs: jobs
      });
    });
  }

};
