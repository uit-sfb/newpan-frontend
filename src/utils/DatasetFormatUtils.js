/**
 * Created by tteige on 30.05.17.
 */

export default {
  urlToFilename(dataset) {
    let parsed = dataset.split('/');
    parsed = parsed[parsed.length - 1];
    parsed = parsed.split('.');
    parsed[1] = parsed[1].split('-');
    parsed = parsed[0] + '.' + parsed[1][0];
    return parsed;
  }
};
