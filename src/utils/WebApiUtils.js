// var client;
import ClientOAuth2 from 'client-oauth2';
import URI from 'urijs';
import Incoming from '../lib/Incoming.js';
import OAuthUtils from './OAuthUtils.js';
import popsicle from 'popsicle';
import AppActions from '../actions/AppActions';

/////////////
/*
 let authServer = 'http://auth.metapipe.uit.no.dev/oauth2';
 let jobsEndpoint = 'http://jobs.metapipe.uit.no.dev';
 let datasetEndpoint = 'http://storage.metapipe.uit.no.dev';

 let incomingHostname = 'incoming.metapipe.uit.no.dev';
 let uploadEndpoint = 'http://upload.metapipe.uit.no.dev/api/frontend/request_upload';
 */
///////////////


var endpoints;

////////////////


var _metapipeAuth;

var _authService = null;

// todo refactor: we are synchronizing state between local storage and _token. Let's not.
var _token = null;
var _introspectionResult = null;

export default {
  createClient() {
    _authService = new ClientOAuth2({
      clientId: 'system_web',
      accessTokenUri: endpoints.authServer + '/token',
      authorizationUri: endpoints.authServer + '/authorize',
      redirectUri: window.location.origin + '/login',
      authorizationGrants: ['token']
    });
  },

  //////////////////// Authentication
  login() {
    if (_authService === null) {
      this.createClient();
    }
    window.oauth2Callback = function (uri) {
      _authService.token.getToken(uri)
        .then((accessToken) => {
          console.log(accessToken);
        });
    };
    window.open(_authService.token.getUri({state: '{"csrf": "cookie", "redirect": "[close]"}'}));
  },

  logout() {
    localStorage.removeItem('accessToken');
    // todo: handle logout properly
    // todo: there should probably be a /logout on the AuthService that invalidates the SAML session
  },


  signIn(username, password) {
    return _metapipeAuth.owner.getToken(username, password)
      .then((token) => {
        _token = token;
        if (localStorage) {
          let creds = {accessToken: _token.accessToken, refreshToken: _token.refreshToken};
          localStorage.setItem('metapipe', JSON.stringify(creds));
          console.log(_token);
        } else {
          console.log('There\'s no local storage!')
        }
        console.log(_token);
        return this.introspect(_token.accessToken);
      }).then((res) => {
        console.log(res);
        _introspectionResult = res;
        return res;
      });
  },

  introspect(accessToken) {
    return _token.request({
      method: 'POST',
      url: endpoints.authServer + '/introspect',
      body: {
        token: accessToken
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    });
  },

  userInfo() {
    return _token.request({
      method: 'GET',
      url: endpoints.authServer + '/userinfo'
    });
  },


  backFromDead(callback) {
    popsicle('/api/endpoints')
      .then((res) => {
        endpoints = res.body;
        console.log(res);
      }).then(() => {
      this.createClient();

      let handleLogin = function (accessToken) {
        console.log("handleLogin: " + accessToken);
        _token = _authService.createToken(accessToken, null, 'Bearer');
        this.userInfo().then(res => {
          console.log(res);
          if (res.status === 200) {
            callback(res.body);
          }
        });

      }.bind(this);

      let onLogin = function (evt) {
        console.log("onLogin");
        if (evt.key === 'accessToken') {
          let accessToken = evt.newValue;
          handleLogin(accessToken);
        }
      };

      window.addEventListener('storage', onLogin, false);

      if (window.location.pathname === '/login') {
        let x = _authService.token.getToken(window.location.href)
          .then((token) => {
            console.log("with-token");
            console.log(token);
            console.log(token.state);
            _token = token;
            localStorage.setItem('accessToken', _token.accessToken);
            console.log(token.data);
            let state = JSON.parse(token.data.state);
            console.log(state);
            if (state.redirect === '[close]') {
              window.close();
            }
            AppActions.navigateTo(state.redirect);
          })
      } else {
        if (localStorage) {
          let accessToken = localStorage.getItem('accessToken');
          if (accessToken) {
            handleLogin(accessToken);
          }
        }
      }
    });
  },

  //////////////////// Jobs
  // todo: handle invalid response

  submitJob(sub, jobId, job) {
    console.log("submitJob");
    console.log(job);

    return _token.request({
      method: 'PUT',
      url: endpoints.jobsEndpoint + '/users/' + sub + '/jobs/' + jobId,
      body: job,
      headers: {
        'Content-Type': 'application/json'
      }
    });
  },

  getJobs(sub) {
    return _token.request({
      method: 'GET',
      url: endpoints.jobsEndpoint + '/users/' + sub + '/jobs'
    }).then((res) => {
      return res.body;
    });
  },


  //////////////////// Datasets

  incomingClient() {
    let incoming = Incoming();
    incoming.set_server_hostname(endpoints.incomingHostname);
    return incoming;
  },

  getUploadToken(filename, url) {
    return _token.request({
      method: 'POST',
      url: endpoints.uploadEndpoint,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: {
        filename: filename,
        uploadUrl: url
      }
    }).then((res) => res.body);
  },

  generateUploadUrl(userSub, filename, tags) {
    let randomString = this.generateId;

    let tagStr = "";

    if (tags) {
      for (let i = 0; i < tags.length; i++) {
        if (i > 0) {
          tagStr += ",";
        }
        tagStr += tags[i];
      }
    }

    let url = filename.substring(0, 40) + '-' + randomString();
    url = url.replace(/[\/\ ]/g, '.');
    url = encodeURIComponent(url) + '?name=' + encodeURIComponent(filename);

    if (tags) {
      url += "&tags=" + tagStr;
    }

    url = endpoints.datasetEndpoint + '/' + userSub + '/inputs/' + url;
    return url;
  },

  getDatasets(sub) {
    return _token.request({
      method: 'get',
      url: endpoints.datasetEndpoint + '/' + sub
    }).then((res) => {
      let datasets = {};
      try {
        for (let key in res.body.objects) {
          if (res.body.objects.hasOwnProperty(key)) {
            let dataset = res.body.objects[key];
            datasets[key] = {
              name: dataset.meta.name,
              description: dataset.meta.description,
              created: dataset.meta.created,
              tags: dataset.meta.tags,
              url: key
            };
          }
        }
      } catch (err) {
        console.log(err)
      }
      return datasets;
    });
  },

  createDownloadLink(datasetUrl) {
    return datasetUrl + '?access_token=' + _token.accessToken;
  },

  generateId() {
    let part1 = (new Date()).getTime().toString(36);
    let part2 = Math.random().toString(36).substring(7);
    return part1 + '-' + part2;
  }
}
