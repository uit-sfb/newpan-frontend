import ClientOAuth2 from 'client-oauth2';

var DEFAULT_HEADERS = {
  'Accept': 'application/json, application/x-www-form-urlencoded',
  'Content-Type': 'application/x-www-form-urlencoded'
};


var ERROR_RESPONSES = {
  'invalid_request': [
    'The request is missing a required parameter, includes an',
    'invalid parameter value, includes a parameter more than',
    'once, or is otherwise malformed.'
  ].join(' '),
  'invalid_client': [
    'Client authentication failed (e.g., unknown client, no',
    'client authentication included, or unsupported',
    'authentication method).'
  ].join(' '),
  'invalid_grant': [
    'The provided authorization grant (e.g., authorization',
    'code, resource owner credentials) or refresh token is',
    'invalid, expired, revoked, does not match the redirection',
    'URI used in the authorization request, or was issued to',
    'another client.'
  ].join(' '),
  'unauthorized_client': [
    'The client is not authorized to request an authorization',
    'code using this method.'
  ].join(' '),
  'unsupported_grant_type': [
    'The authorization grant type is not supported by the',
    'authorization server.'
  ].join(' '),
  'access_denied': [
    'The resource owner or authorization server denied the request.'
  ].join(' '),
  'unsupported_response_type': [
    'The authorization server does not support obtaining',
    'an authorization code using this method.'
  ].join(' '),
  'invalid_scope': [
    'The requested scope is invalid, unknown, or malformed.'
  ].join(' '),
  'server_error': [
    'The authorization server encountered an unexpected',
    'condition that prevented it from fulfilling the request.',
    '(This error code is needed because a 500 Internal Server',
    'Error HTTP status code cannot be returned to the client',
    'via an HTTP redirect.)'
  ].join(' '),
  'temporarily_unavailable': [
    'The authorization server is currently unable to handle',
    'the request due to a temporary overloading or maintenance',
    'of the server.'
  ].join(' ')
}
var _hasOwnProperty = Object.prototype.hasOwnProperty;

function auth (username, password) {
  return 'Basic ' + btoa(string(username) + ':' + string(password))
};

function assign (dest /*, ...source */) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i]

    for (var key in source) {
      if (_hasOwnProperty.call(source, key)) {
        dest[key] = source[key]
      }
    }
  }

  return dest
};

function string (str) {
  return str == null ? '' : String(str)
};

function handleAuthResponse (res) {
  var data = res.body
  var err = getAuthError(data)

  // If the response contains an error, reject the refresh token.
  if (err) {
    return Promise.reject(err)
  }

  return data
};



function getAuthError (data) {
  var message = ERROR_RESPONSES[data.error] ||
    data.error ||
    data.error_message

  // Return an error instance with the message if it exists.
  return message && new Error(message)
};

export default {
  refreshTokenGrant(ctok, refreshToken, scope) {
    var options = ctok.client.options;

    return ctok.client._request({
      url: options.accessTokenUri,
      method: 'POST',
      headers: assign({
        Authorization: auth(options.clientId, options.clientSecret)
      }, DEFAULT_HEADERS),
      body: {
        refresh_token: ctok.refreshToken,
        grant_type: 'refresh_token',
        scope: scope
      }
    }).then(handleAuthResponse);
  }
};
