import ApiUtils from './WebApiUtils.js'

export default {
  clickHandler: function(event) {
    // If not left mouse click
    if (event.button !== 0) {
      return;
    }
    // If modified event
    if (event.metaKey || event.altKey || event.ctrlKey || event.shiftKey) {
      return;
    }
    event.preventDefault();
    console.log("HELLO LOGIN CLICK HANDLER");

    ApiUtils.login();
  }
};
